var express = require('express');
var router = express.Router();
var auth = require('./auth');
var job = require('./jobs');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'FOP API' });
});

//Externalized the business logic for the route
router.post('/login', auth.login);

//Externalized the business logic for the job
router.get('/api/v1/jobs', job.getAll);
router.get('/api/v1/jobs/:id',job.getById);
router.post('/api/v1/jobs',job.create);
router.put('/api/v1/jobs/:id', job.updateById);
router.delete('/api/v1/jobs/:id', job.deleteById);

module.exports = router;
