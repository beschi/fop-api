var Job = require('../models/Job');
const jobs = {
    getAll: function(req, res, next){
        Job.find(function(err, jobs){
            if(err) return next(err);
            res.json(jobs);
        });
    },
    
    getById:function(req, res, next){
        Job.findById(req.params.id, function(err, job){
            if(err) return next(err);
            res.json(job);
        });
    },
    
    create:function(req, res, next){
        Job.create(req.body, function(err,job){
            if(err) return next(err);
            res.json(job);
        });
    },
    
    deleteById:function(req, res, next){
        Job.findByIdAndRemove(req.params.id, function(err, job){
            if(err) return next(err);
            res.json(job);
        });
    },
    
    updateById:function(req, res, next){
        Job.findByIdAndUpdate(req.params.id, req.body, function(err, job){
            if(err) return next(err);
            res.json(job)
        });
    }
}
module.exports = jobs;