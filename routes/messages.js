var express = require('express');
var router = express.Router();
var Message = require('../models/Message');

/*GET retrives all messages */
router.get('/',function(req, res, next){
    Message.find(function(err, msgs){
        if(err) return next(err);
        res.json(msgs);
    });
});

/*GET /msgs/id retrives message by given id */
router.get('/:id',function(req, res, next){
    Message.findById(req.params.id, function(err, msg){
        if(err) return next(err);
        res.json(msg);
    });
});

/*POST /msgs - creates new message*/
router.post('/', function(req, res, next){
    Message.create(req.body, function(err, msg){
        if(err) return next(err);
        res.json(msg);
    });
});

/*PUT /msgs/id  updates the message */
//findOneAndUpdate({ postedBy: 'beschi' }, { postedBy: 'antony' }, function(err, msg)
router.put('/:id', function(req, res, next){
    Message.findByIdAndUpdate(req.params.id, req.body, function(err, msg){
        if(err) return next(err);
        res.json(msg);
    });
});

/*DELETE /msgs/id deletes the message with the given id */
//findOneAndRemove({posetedBy:'beschi'}, function(err,msg))
router.delete('/:id', function(req, res, next){
    Message.findByIdAndRemove(req.params.id, function(err, msg){
        if(err) return next(err);
        res.json(msg);
    });
});

module.exports = router;
