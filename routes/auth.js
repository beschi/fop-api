var jwt = require('jwt-simple');
var auth = {
    login: function(req, res){
        console.log(req.body)
        var username = req.body.username || '';
        var password = req.body.password || '';
        if(username == '' || password == ''){
            res.status(401);
            res.json({
                "status":401,
                "message":"Invalid Credentials"
            });
            return;
        }

        var authenticatedUser = auth.validateCredentials(username, password);

        if(!authenticatedUser){ //If authentication fails
            res.status(401);
            res.json({
                "status":401,
                "message":"Invalid Credentials"
            });
            return;
        } else {
            return res.json(getToken(authenticatedUser));
        }
    },

    validateCredentials: function(username, password){
        var userObj={
            name:"beschi",
            rold:"admin",
            username:"beschi@am.com"
        }
        return userObj;
    },

    validateUser: function(username){
        var userObj={
            name:"beschi",
            rold:"admin",
            username:"beschi@am.com"
        }
        return userObj;
    },
}

//Local or private functions
function getToken(user){
    var expires = expiresIn(7);
    var token = jwt.encode({exp:expires}, require('../config/secret')());
    return {
        token: token,
        expires: expires,
        user : user
    }
}

function expiresIn(numOfDays){
    var dateObj = new Date();
    return dateObj.setDate(dateObj.getDate() + numOfDays);
}

module.exports = auth;