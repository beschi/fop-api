var express = require('express');
var router = express.Router();
var Event = require('../models/Event');

/* GET events listing. */
router.get('/', function(req, res, next) {
  Event.find( function(err, events){
    if(err) return next(err);
    res.json(events);
  }
  );
});

/* GET events/id listing. 
  findById(): Finds the document with the specified id, 
  findOne(): Finds a single document that matches the specified criteria.*/
router.get('/:id', function(req, res, next) {
  Event.findById(req.params.id, function(err, event){
    if(err) return next(err);
    res.json(event);
  }
  );
});

/*Event.
  find().
  where('name').equals('BirthDay').
  //where('age').gt(17).lt(50).  //Additional where query
  limit(5).
  sort({ age: -1 }).
  select('name age').
  exec(callback); // where callback is the name of our callback function.*/

/* POST creates event */
router.post('/', function(req, res, next){
  Event.create(req.body, function (err, post){
    if(err) return next(err);
    res.json(post);
  });
});

/* PUT /events/:id  updates the given event*/
router.put('/:id', function(req, res, next){
  console.log(req.body);
  Event.findByIdAndUpdate(req.params.id, req.body, function(err, event){
    if(err) return next(err);
    res.json(event);
  });
});

/* DELETE /events/:id deletes the given event 
findOneAndRemove() method also can be used to find based on criteria */
router.delete('/:id', function(req, res, next){
  Event.findByIdAndRemove(req.params.id, function(err, event){
    if(err) return next(err);
    res.json(event);
  });
});

module.exports = router;
