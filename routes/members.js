var express = require('express');
var router = express.Router();
var Member = require('../models/Member');
var multer  = require('multer');
var path = require('path');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now()+file.originalname)
  }
})
 
var upload = multer({ storage: storage })

/**
 * GET all members
 */
router.get('/', function(req, res, next){
    Member.find(function(err, members){
        if(err) return next(err);
        res.json(members);
    });
});

/**
 * GET member by Id
 */
router.get('/:id', function(req, res, next){
    Member.findById(req.params.id, function(err, member){
        if(err) return next(err);
        res.json(member);
    });
});


/**
 * POST creates a new member
 */
router.post('/', upload.single('userPhoto'), function(req, res, next){
    req.body.profilePic=req.protocol + '://' + req.get('host')+"/static/uploads/"+req.file.filename;
    Member.create(req.body, function(err, member){
        if(err) return next(err);
        res.json(member);
    });
});

/**
 * PUT, updates details of the user with the given id
 */
router.put('/:id', function(req, res, next){
    Member.findByIdAndUpdate(req.params.id,req.body, function(err, member)
    {
        if(err) return next(err);
        res.json(member);
    });
});

/**
 * DELETE, deletes the member of the given id.
 */
router.delete('/:id', function(req, res, next){
    Member.findByIdAndRemove(req.params.id, function(err, member){
        if(err) return next(err);
        res.json(member)
    });
});

module.exports = router;