/**
 * Creates Event Schema for mongoose
 */

var mongoose = require('mongoose');

var LandMarkSchema = new mongoose.Schema(
    {
        lm:String
    }
);

var ContactSchema = new mongoose.Schema(
    {
        name:String,
        mobile:String,
        email:String
    }
);

var EventSchema = new mongoose.Schema(
    {
        name:String,
        startDate:Date,
        endDate:Date,
        place:String,
        landMarks:[LandMarkSchema],
        route:String,
        contacts:[ContactSchema],
        createdBy:String,
        createdOn:{type:Date, default:Date.now}
    }
);

module.exports = mongoose.model('Event',EventSchema);