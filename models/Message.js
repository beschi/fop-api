/**
 * Creates Message schema for mongoose
 */  

var mongoose = require('mongoose');
var Comments = new mongoose.Schema({
    comment:String,
    commentedBy:String,
    commentedOn:Date
});

var messageSchema = new mongoose.Schema({
    postedBy: String,
    message : String,
    image: String,
    postedOn: {type:Date, default:Date.now},
    likes:Number,
    dislikes:Number,
    comments:[Comments]
});

//Create model from schema
var Message = mongoose.model('Message', messageSchema);

//Exports to applications
module.exports = Message;