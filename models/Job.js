var mongoose = require('mongoose');

var ContactSchema = new mongoose.Schema(
    {
        name:String,
        mobile:String,
        email:String
    }
);

var JobSchema = new mongoose.Schema({
    title : String,
    workLocation:String,
    interviewLocation:String,
    interviewDate:Date,
    contacts:[ContactSchema],
    postedBy:String,
    postedOn:{type:Date, default:Date.now},
    updatedOn:Date
});

module.exports= mongoose.model('Job',JobSchema);