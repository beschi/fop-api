var monoose = require('mongoose');
var MemberSchema = new monoose.Schema({
    username:String,
    password:String,
    firstname:String,
    lastname:String,
    dob:Date,
    profession:String,
    location:String,
    email:String,
    mobile:String,
    phone:String,
    profilePic:String,
    Address:{
        doorOrHouseNumber:String,
        streetName:String,
        city:String,
        state:String,
        pincode:String
    }
});

module.exports = monoose.model('Member', MemberSchema);

