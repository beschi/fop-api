var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');


var index = require('./routes/index');
var events = require('./routes/events');
var msgs = require('./routes/messages');
var members = require('./routes/members');
var authenticateRequests = require('./middleware/authenticateRequest');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(logger('dev'));
app.use(cookieParser());
app.use('/static',express.static(path.join(__dirname, 'public')));

app.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});


app.use('/', index);
app.all('/api/v1/*',[authenticateRequests]);
app.use('/api/v1/events', events);
app.use('/api/v1/msgs', msgs);
app.use('/api/v1/members', members);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//Use mongoose promise
mongoose.Promise = global.Promise;
//Connect to mongoose
//var dbURL='mongodb://tester:tester@localhost/test';//local installation
var dbURL='mongodb://fopuser:fopuser@ds159371.mlab.com:59371/fopapi'; //cloud environment
mongoose.connect(dbURL)
        .then(()=>console.log('Connection to MongoDB is Successful'))
        .catch((err)=>console.error(err));

module.exports = app;
